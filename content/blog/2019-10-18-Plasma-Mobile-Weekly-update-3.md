---
author: Plasma Mobile team
created_at: 2019-10-18 15:00:00 UTC+2
date: "2019-10-18T00:00:00Z"
title: 'Plasma Mobile: weekly update: part 3'
---

Thanks to the awesome KDE community, we are happy to present the Plasma Mobile project's third weekly update.

## Shell User Interface

David Edmundson backported some Qt Wayland patches that improve the overall stability of the shell.

Marco Martin added further refinements to the shell.

## Kaidan

Jonah Brüchert replaced the custom contacts search field by the recently added Kirigami search field in Kaidan. The search is accessible by using the search action or by pressing Ctrl+F as usual. Robert Maerkisch improved the performance of the search by using a proxy model instead of hiding contacts from QML. Apart from that the contacts are now sorted chronologically and secondarily also alphabetically thanks to Linus Jahn.

![New Kaidan contacts view](/img/screenshots/kaidan_contacts.png){: .blog-post-image-small}

## Kirigami

Kirigami apps no longer show an empty context drawer in certain circumstances (Nicolas Fella).

Marco Martin fixed the Kirigami drawer handles on Plasma Mobile.

## KBibTeX

Thomas Fischer has started work on a [mobile-friendly BibTeX client](https://t-fischer.dreamwidth.org/9049.html).

## Dialer

Nicolas Fella made sure that an appropriate message is shown when no contacts could be found.

## Settings Application

The code for the settings applications received a small cleanup. Names of various settings entries have been adjusted to be more concise: they now show "Foo" instead of "Configure Foo" in the top bar.

## Calindori

When creating or editing an entry, saving is now the most prominent action.

## Angelfish
Angelfish, the browser, has seen some refactoring by Jonah Brüchert which avoids loading all kinds of dialogs (authentication, webrtc permissions etc) at startup and loads them when needed. This should result in reduced startup times.

Input of urls in its settings is now converted into proper urls, to make sure for example the homepage can actually be loaded even though the user did not enter https://kde.org but just kde.org.

## Maui Project

The file browser component has gained improvements for focus chaining, keyboard navigation and new keyboard shortcuts. The places list can now browse all available tags. Files now keep their associated tags even when renamed or moved. Furthermore it now supports copy/pasting files into/out of the app.

The file previewer now uses Okular to preview any document supported by Okular.

The browser view has an improved Miller Column view and its model features improved performance by only adding or removing the necessary paths.

The MauiKit framework and some Maui apps can now be built for Android using CMake in addition to qmake. This allows building [Index](https://binary-factory.kde.org/view/Android/job/Index_android/) and [vvave](https://binary-factory.kde.org/view/Android/job/vvave_android/) using the KDE Binary Factory and making them available in the KDE [F-Droid repository](https://community.kde.org/Android/FDroid) (Nicolas Fella). This allows you to test some of the Plasma Mobile apps even without a Plasma Mobile device.
MauiKit gained initial support for handling system accounts in a more integrated way by using libaccounts on Linux.

MauiKit parts now have been separated into components to allow having lighter builds when using static linking. Those components are: Editor, Filemanager, Tagging, Syncing, Accounts, Terminal and Store.

When using qmake, MauiKit now automatically fetches all the needed extra binaries, library sources, and assets needed to compile when statically linking.

vvave's codebase has received some cleanups and improvements.

## Want to be part of it?
Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
