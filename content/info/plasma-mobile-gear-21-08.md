---
version: "21.08"
title: "Plasma Mobile Gear 21.08"
type: info
date: 2021-08-31"
---

In this release following repositories are marked as unstable,

- kalk
- kclock
- krecorder
- kweather
- qmlkonsole
- tokodon

Some of this repositories are in process of kdereview, and will be marked stable in upcoming releases.

# Changelog

{{< details id="angelfish" title="angelfish" link="https://commits.kde.org/angelfish" >}}
+ Make sure icon and desktop file directory exist. [Commit.](http://commits.kde.org/angelfish/2d1473fecb55bdb8c18f0c463244c7f98ed16060) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/54f06d5771117f82d9bc447afde2d7ac5829b38a) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/1132102184057b5776c02a6a1982eaba2024dd43) 
+ Regenerate flatpak sources. [Commit.](http://commits.kde.org/angelfish/912496b68f1efdf37eff129538c8c69e1738b4c7) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/dbb4920154dec662b7f0d9c34944374385f46282) 
+ Add X-KDE-FormFactor to .desktop file. [Commit.](http://commits.kde.org/angelfish/a434e2784e396ee66d9cabdefa559c0b905a3d9b) 
+ README.md: Add build instructions. [Commit.](http://commits.kde.org/angelfish/6e6640233ba4374b84e6ca4e3a89eed32d026234) 
+ Add developer tools. [Commit.](http://commits.kde.org/angelfish/76419ba2e09f81a15a19973d85f97561bbd0aaab) 
{{< /details >}}

{{< details id="audiotube" title="audiotube" link="https://commits.kde.org/audiotube" >}}
+ New in this release
{{< /details >}}

{{< details id="kalk" title="kalk" link="https://commits.kde.org/kalk" >}}
+ Indentation. [Commit.](http://commits.kde.org/kalk/7f968c3ab26b0ce47ed734158d63afe48751c11e) 
+ Added U+2212 trigger. [Commit.](http://commits.kde.org/kalk/6499c5ed8c4598e001f5b462d1247a57120ee162) 
+ Added more keyboard triggers for multiplication and division. [Commit.](http://commits.kde.org/kalk/19491a1847830b014d0461a7a415ad909bd6f10f) 
+ Use KDE notation for X-KDE-FormFactor. [Commit.](http://commits.kde.org/kalk/9bb86f286c182d486ea06c211b26f7966b3a1dc2) 
+ Add X-KDE-FormFactor. [Commit.](http://commits.kde.org/kalk/e5807786bc12c0d80a2bf4421c3bc61df561bea7) 
{{< /details >}}

{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ Add 21.08 release changelog to appdata. [Commit.](http://commits.kde.org/kasts/c0fff75d6b1d17ca5be5e06eb8451a61ef7cd17c) 
+ Select first enclosure if entry has multiple enclosure entries. [Commit.](http://commits.kde.org/kasts/0b33c62a108f3789e679ee357284f4ea206320a5) See bug [#440389](https://bugs.kde.org/440389)
+ Fix bug with feeds with multiple enclosures per entry. [Commit.](http://commits.kde.org/kasts/6eb13167f2e39cb0c43a5cec2cc68a0249b166d7) Fixes bug [#440389](https://bugs.kde.org/440389)
+ Add X-KDE-FormFactor. [Commit.](http://commits.kde.org/kasts/00be5e38721d92d6d57caf65d38e6dfd8e75e99d) 
+ Fix minor typo. [Commit.](http://commits.kde.org/kasts/9384f3c073cd9dd4536ae264d9376ba7fe0fc80b) 
+ Make storage path configurable. [Commit.](http://commits.kde.org/kasts/a15e2dbe5d232e20ccc270b62fb8c4b399a29f56) 
+ Run clang-format. [Commit.](http://commits.kde.org/kasts/85798ebd8cada3a2b228076b5691b63ee929f7d1) 
+ Error handling for PodcastSearchModel. [Commit.](http://commits.kde.org/kasts/d0dbdad7fafcb35ff5d0989cc53b3da1f9fb0264) 
{{< /details >}}

{{< details id="kclock" title="kclock" link="https://commits.kde.org/kclock" >}}
+ Require CMake 3.16. [Commit.](http://commits.kde.org/kclock/03f1e7962d5ddedd7f872450cceed5820f7da9cb) 
+ Don't create full timezone model until needed. [Commit.](http://commits.kde.org/kclock/bbed814870426e0982d4308bc1312cac3a3056d6) 
+ Extract saved timezones into a separate model. [Commit.](http://commits.kde.org/kclock/ce73be7ef3529f372f0158313f4cf328eb43ff18) 
+ Add list of timer ids to DBus interface. [Commit.](http://commits.kde.org/kclock/bb1d00e187341c448a50dabf0e195ff4a2f34e0b) 
+ Use KDE notation for X-KDE-FormFactor. [Commit.](http://commits.kde.org/kclock/64c5008e3342551d60a593ff091b56940d91e38b) 
+ Add X-KDE-FormFactor. [Commit.](http://commits.kde.org/kclock/f14f55bb19bfa8640601a2243df507d0fb30ee91) 
+ Run clock animation each page open. [Commit.](http://commits.kde.org/kclock/56f360a4b57c72df8ea3ebdffbc274ef7cb20cfc) 
+ Enhance the visuals of the icon next to the command field in the TimerPage. [Commit.](http://commits.kde.org/kclock/9ab690fec8d5e70b83a78b7f88b9a23dcf1fb0f2) 
+ Enhance the visuals of the icon next to the command field in the TimerListPage. [Commit.](http://commits.kde.org/kclock/5ad41eed1c500135be342682d0a8feceec7b00cd) 
+ Make the placeholder message more fitting. [Commit.](http://commits.kde.org/kclock/62d4652cc4dbe58152fe254fbfe6d075278ef531) 
+ Replace a Kirigami.Label with a Label. [Commit.](http://commits.kde.org/kclock/baac8acdea0ea137254d6e0aabf84125dc90ba21) 
+ Simplification of the command at timeout feature. [Commit.](http://commits.kde.org/kclock/2155e7d77546d734d1275573a4976838675318e3) 
+ Add button to enable commands on the TimerPage. [Commit.](http://commits.kde.org/kclock/aaf3cc07655f19817a9506bf5af59623f7fc0b55) 
+ Add ability to run a command at timer's timeout. [Commit.](http://commits.kde.org/kclock/89af15c17d2bef22ea1d11e8b5b50ed3124063c2) 
+ Add clock intro animation. [Commit.](http://commits.kde.org/kclock/bbfc13173e09152f1877e6a6b784b15c30225029) 
+ Enhance the visuals of the icon next to the command field in the TimerPage. [Commit.](http://commits.kde.org/kclock/17df2f816878246e39a06034f9214ecd1506e1eb) 
+ Enhance the visuals of the icon next to the command field in the TimerListPage. [Commit.](http://commits.kde.org/kclock/c3e274ab21aa00cb4fbe6ce101c6a01c501af79c) 
+ Make the placeholder message more fitting. [Commit.](http://commits.kde.org/kclock/6e39681a0a441270e3333975207ffaea5bbe5a65) 
+ Replace a Kirigami.Label with a Label. [Commit.](http://commits.kde.org/kclock/210915808d46e676d7571c6a146f7ba983b7fef5) 
+ Simplification of the command at timeout feature. [Commit.](http://commits.kde.org/kclock/3e901bc2b30457dffbe716109ffceff4b337501e) 
+ Add ability to loop timer. [Commit.](http://commits.kde.org/kclock/c9a406181f823ec6597448035b843b1f41458204) 
+ Add button to enable commands on the TimerPage. [Commit.](http://commits.kde.org/kclock/338d3ba8ca1a0143dfdec68ea5f4c28502d9117f) 
+ Add ability to run a command at timer's timeout. [Commit.](http://commits.kde.org/kclock/e823410c779e841e98ecbe1ca1a7904519726c28) 
{{< /details >}}

{{< details id="keysmith" title="keysmith" link="https://commits.kde.org/keysmith" >}}
+ Test: bump limits to trigger confusion in QDateTime. [Commit.](http://commits.kde.org/keysmith/3c0fc6201f65363eceeb24339e525bba2965f5e3) 
+ Chore: code quality fixups suggest by CI. [Commit.](http://commits.kde.org/keysmith/7fbad71cc3c6938c2efaf1a93f45dcc1ae104dad) 
+ Feat: update flatpak manifest. [Commit.](http://commits.kde.org/keysmith/503fddece6b14b3c96038fb2ffdbffa6bd190729) 
{{< /details >}}

{{< details id="koko" title="koko" link="https://commits.kde.org/koko" >}}
+ Add InfoSidebar, InfoDrawer and TagInput. [Commit.](http://commits.kde.org/koko/d3c4c4d64147805af142d23073df595b497e764e) 
+ Tag: Use pointy shape. [Commit.](http://commits.kde.org/koko/5741308ed060179082f32b040bb4b0706d2da0ed) 
+ Sort qrc file alphabetically. [Commit.](http://commits.kde.org/koko/9782ea750af37966764dbbfcf8734762b4ab4980) 
+ Rename ImageViewer to ImageViewPage. [Commit.](http://commits.kde.org/koko/4fa41fe0caed8dad55350c3fe96bca48827a6a8a) 
+ Use custom element to show SVGs instead of images. [Commit.](http://commits.kde.org/koko/9b48c5a90bd520a5a1e4bb9b1f20b2ac2ccd773e) 
+ Fix spacing of overlay button from back button. [Commit.](http://commits.kde.org/koko/f4424407a11812e78384fb9b0d7fee91d54edcca) 
+ Set minimum size for window. [Commit.](http://commits.kde.org/koko/0e4c9e7c0fb7238544367a60f01c23e604ed9201) 
+ VideoPlayer: set implicit size, expose Video status as a property. [Commit.](http://commits.kde.org/koko/bad9d211e4196d632fc322d0b903963a14c3f1c4) 
+ ImageViewer: Add Mikel Johnson's fix for forward/back mouse buttons. [Commit.](http://commits.kde.org/koko/786d425597d2b4a4b24b8c9245722e48a4574500) 
+ ImageViewer: Use ListView's functions for incrementing/decrementing. [Commit.](http://commits.kde.org/koko/fd863e07c4e2ac9586dbf723553be128a64ac85c) 
+ ImageViewer: Don't show other images when resizing the ListView. [Commit.](http://commits.kde.org/koko/72b30239342716ccad46cd9af83c413d052d1f50) 
+ ImageViewer: enable pixelAligned for ListView. [Commit.](http://commits.kde.org/koko/dc446f3f0b3bb776c4259a1d9740aedd4a38ed47) 
+ ImageViewer: Make ListView not interactive when currentItem is. [Commit.](http://commits.kde.org/koko/584ecb430ccbff61e68961a598d0b8c0608d469a) 
+ ImageViewer: Make all floating controls disappear when dragging or using overview. [Commit.](http://commits.kde.org/koko/b70b34b7f26d5484ace4d29e4fa2350756d40407) 
+ ImageViewer: Add OverviewControl. [Commit.](http://commits.kde.org/koko/c85912f5cce8d322ac17718e8290b50a2ff8e223) 
+ ImageViewer: Move BusyIndicator to ListView. [Commit.](http://commits.kde.org/koko/f5c3c72a2f1c32f034882e58efabf5730d39e394) 
+ Rewrite ImageDelegate without using Flickable, plus tweaks. [Commit.](http://commits.kde.org/koko/27d430103a5031aa553b74f8b9d95638eaed645e) 
+ Add OverviewControl. [Commit.](http://commits.kde.org/koko/fec8764d6d33e8e63076d4fea2189123f7d78139) 
+ Use PreserveAspectFit for thumbnails instead of PreserveAspectCrop. [Commit.](http://commits.kde.org/koko/d4dacb83b9def1bac16ef292512f739db8990c11) 
+ Use Shortcut for key navigation of the main image view. [Commit.](http://commits.kde.org/koko/265456b1ba924dfa2bb346f153531362bb89067a) 
+ Implement open/closed hand cursor for ImageDelegate. [Commit.](http://commits.kde.org/koko/1b02c9d27252b11babc1e77990fb9d7e512933cd) 
+ Reduce duration of image zooming animation. [Commit.](http://commits.kde.org/koko/8b5d939e584161fc79e1afc9b8e4ad1c8a2c3c14) 
+ Prefer zooming over scrolling in wheel handling code of ImageDelegate. [Commit.](http://commits.kde.org/koko/25f01deccaa3437ab536becdcbafa45ee429c347) 
{{< /details >}}

{{< details id="krecorder" title="krecorder" link="https://commits.kde.org/krecorder" >}}
+ Use KDE notation for X-KDE-FormFactor. [Commit.](http://commits.kde.org/krecorder/fd5e12aa64a2b0c34797820e717e8deef56325d9) 
+ Add X-KDE-FormFactor. [Commit.](http://commits.kde.org/krecorder/e25a42f7ba5c8024e09911e92eaef2e8ed82c512) 
{{< /details >}}

{{< details id="ktrip" title="ktrip" link="https://commits.kde.org/ktrip" >}}
+ Port away from KPublicTransport API deprecated since 21.08 or before. [Commit.](http://commits.kde.org/ktrip/b5a7eb80adaa6080022f6e72c0e5a04b1c60a3c1) 
+ Fix setting icon theme fallback. [Commit.](http://commits.kde.org/ktrip/7317bbf86b5afd2297103005bed8341a77ba9903) 
+ Set fallback icon theme to breeze. [Commit.](http://commits.kde.org/ktrip/0a2eba252d6c220a72454360fb55d5477a437b0c) 
+ Add formfactor to desktop file. [Commit.](http://commits.kde.org/ktrip/8e66819d539c3e6794ab00f63e912ef565589a18) 
+ Remove unneeded breeze icons dir stuff. [Commit.](http://commits.kde.org/ktrip/00ec89e112b46a3470e9f264a7d37dbb040659a3) 
+ Set width for journey section delegates. [Commit.](http://commits.kde.org/ktrip/31d77c51c611991bfb4c35a97e94856d8973cada) 
{{< /details >}}

{{< details id="kweather" title="kweather" link="https://commits.kde.org/kweather" >}}
+ Consistently order includes. [Commit.](http://commits.kde.org/kweather/35406b1436bef9d82ed3e24296e9f3b8d5729877) 
+ Fix KWeatherCore cmake check. [Commit.](http://commits.kde.org/kweather/1c240a3c60c8350bc5df64ff1f0203f6b03a9773) 
+ Have one declaration per line. [Commit.](http://commits.kde.org/kweather/950d9d19f7c94a0c18f63118b007389e6c27981c) 
+ Remove unused struct. [Commit.](http://commits.kde.org/kweather/c38e7fe503a54a0202563b148ea574f7879ff75c) 
+ Make it build against latest KDECompilerSettings. [Commit.](http://commits.kde.org/kweather/8d705fab9e35b1354b45fd09eeeae03d4f03afe9) 
+ Remove unused constants. [Commit.](http://commits.kde.org/kweather/8c5d9cfcf5fe5436431c0c29aa939cce00feb7b0) 
+ Fix switching unit at runtime. [Commit.](http://commits.kde.org/kweather/8cf33784dfe8135b1793eb68ffb8c8dc9c38737c) 
+ Update WeatherLocation on creation. [Commit.](http://commits.kde.org/kweather/7502ddf159b52755f98c9482b63f50e5a66a5358) 
+ Remove empty method. [Commit.](http://commits.kde.org/kweather/d01a93344e52d08749afa615a576be463f50ad06) 
+ Remove duplicate include. [Commit.](http://commits.kde.org/kweather/c0f239dad847948384855e30663a15dbd8340b21) 
+ Make KWeatherSettings a singleton. [Commit.](http://commits.kde.org/kweather/ab4bf790d53f18787465f638ae6bb0617d2026e8) 
+ Don't load LocationQueryModel until needed. [Commit.](http://commits.kde.org/kweather/fd6d133f6cf0271980ca42bc192667909721dc3a) 
+ Add myself to copyright headers. [Commit.](http://commits.kde.org/kweather/9118a5077c0d3324377013734b518eb361c2e5b8) 
+ Fix WeatherStrip arrows. [Commit.](http://commits.kde.org/kweather/db530ebafca73b9161a230c5edc276b36b31f36e) 
+ Properly format stuff. [Commit.](http://commits.kde.org/kweather/18211320ac249c14fb94ed89db9316c86190ce26) 
+ Fix locations page. [Commit.](http://commits.kde.org/kweather/6dc3a9332cfd114e31f4d94fa55a840a3b0ae4aa) 
+ Split chart data handling into separate file. [Commit.](http://commits.kde.org/kweather/0b1ce216c4ab18939229239df60b16f2c1e460f8) 
+ Simplify hours handling. [Commit.](http://commits.kde.org/kweather/bb02053ab7b067882e05a24685ff6a4f47701af9) 
+ Remove unneeded QML type registration and constructor. [Commit.](http://commits.kde.org/kweather/53c696cc5f48578eb440efae4506cb888eed21a7) 
+ Introduce WeatherStrip component. [Commit.](http://commits.kde.org/kweather/7d87b82cb8e1fcabf95768571e249c4fb856dbaf) 
+ Prevent crash when forecast is empty. [Commit.](http://commits.kde.org/kweather/b76593d12da590f85c7fa280c2eb11db4c9c2928) 
+ Extract information card into own QML file. [Commit.](http://commits.kde.org/kweather/0b795185e6c4a2018733573f26f97a59b80e507e) 
+ Add compact representation to plasmoid. [Commit.](http://commits.kde.org/kweather/1484243c2a76a3761a98e92cd8f6be7155df4add) 
+ Fix temperature chart from having tons of duplicate data. [Commit.](http://commits.kde.org/kweather/dccfaa0fea063c5a79b1f8d41261e5532e5387dc) 
+ Fix temperature and sunrise displays. [Commit.](http://commits.kde.org/kweather/ea794a02bedc85f7a013427fe1f34a807a85e5d0) 
+ Factor sunrise card into separate QML file. [Commit.](http://commits.kde.org/kweather/291b2f0cabefdc6cae818d5d41fa0646947ecfa2) 
+ Split temperature chart into separate QML component. [Commit.](http://commits.kde.org/kweather/ca5b9804984cf51c52830fbb0105c685f44c698a) 
+ Simplify day forecast code. [Commit.](http://commits.kde.org/kweather/800fd62d2beb124c2fd439b7c73393aa92cad072) 
+ Add outbound license test. [Commit.](http://commits.kde.org/kweather/bcda9df404c1bb2da032f9975c66ec310b6b6b6a) 
+ Add missing include. [Commit.](http://commits.kde.org/kweather/2bd8db1237a84b176f2c7479b820cd3ed2dd5de1) 
+ Simplify day forecast code. [Commit.](http://commits.kde.org/kweather/f1d09b5a96baa05073e7807fcd6dec2b5459c1bc) 
+ Run clang-format and add commit hook. [Commit.](http://commits.kde.org/kweather/db1dc35991912f44bc80cac4a00f7a6ee9e46305) 
+ Convert copyright statements to SPDX. [Commit.](http://commits.kde.org/kweather/819b35721bdc52dc556d8880490cb01c5ef6c835) 
+ Make sure linked website actually exists. [Commit.](http://commits.kde.org/kweather/55a14595b7180009487f0f7421e79697b80883cb) 
+ Adapt to KWeatherCore API change. [Commit.](http://commits.kde.org/kweather/4f9aebf0621d0e83bcf77aa7614fc146859cd5cb) 
+ Port back old qtcharts code. [Commit.](http://commits.kde.org/kweather/d70d0118f7772e6262bf750a6aa6f29971fa0187) 
+ Avoid unneeded copies. [Commit.](http://commits.kde.org/kweather/a8764f3b4d49e54fc3dd779bdf4b31c9b20ec6a4) 
+ Show a location's State if it located in the U.S. [Commit.](http://commits.kde.org/kweather/fd019caa701fbf55a27462a5f2cd68fd9d8b6246) 
+ Use KDE notation for X-KDE-FormFactor. [Commit.](http://commits.kde.org/kweather/15ebb4f01143a1e7402da06590f7ff474bf25f6e) 
+ Add X-KDE-FormFactor. [Commit.](http://commits.kde.org/kweather/0a853f044fbf6f52f566c5b6193493802358cc47) 
+ Add flathub badge. [Commit.](http://commits.kde.org/kweather/b0946d156ebaf88242e0325b4a3a7a7df603c213) 
+ Convert daily temp chart and hourly display units properly. [Commit.](http://commits.kde.org/kweather/5726226406ecedc5738753d73c9a6e30720a2240) 
+ Don't link against QtWidgets on Android. [Commit.](http://commits.kde.org/kweather/507624162d9e0c85f74d0f274e461caf05807ca1) 
{{< /details >}}

{{< details id="plasma-dialer" title="plasma-dialer" link="https://commits.kde.org/plasma-dialer" >}}
+ Add license file for BSD-3-Clause. [Commit.](http://commits.kde.org/plasma-dialer/de002cf974af12c20587ecb7139d24ec23a02bfe) 
+ Add unit test for contactmapper. [Commit.](http://commits.kde.org/plasma-dialer/8cf8bfb9db7995da0f24d003d283bccbcbe18c04) 
{{< /details >}}

{{< details id="plasma-phonebook" title="plasma-phonebook" link="https://commits.kde.org/plasma-phonebook" >}}
+ Remove unused QtQuick.Dialogs import. [Commit.](http://commits.kde.org/plasma-phonebook/c712939a8a0e7504ea3d543bcda3a4f6ac459a86) 
{{< /details >}}

{{< details id="plasma-settings" title="plasma-settings" link="https://commits.kde.org/plasma-settings" >}}
+ Update timezone i18n country and city names. [Commit.](http://commits.kde.org/plasma-settings/e9abd2ca87c39ab22c300bdde80b6adebf28d30e) 
+ Password: allow text passwords. [Commit.](http://commits.kde.org/plasma-settings/7f74746678c2ce9fdd1bafe5b4292625a9330b66) 
{{< /details >}}

{{< details id="spacebar" title="spacebar" link="https://commits.kde.org/spacebar" >}}
+ Mark messages as Failed on DBus and Ofono errors. [Commit.](http://commits.kde.org/spacebar/6d59c7beb745a86e35d252a2a7e46fe74d076bbf) 
+ Move delivery status icon outsite of chat bubble. [Commit.](http://commits.kde.org/spacebar/fcc781124bbb6a3f724e037f9706c633f180e5e9) 
+ Increased padding per the recommendation of the Visual Design Group. [Commit.](http://commits.kde.org/spacebar/c80303cb36385c9e66a30c7d1cc30731465b090e) 
+ Improve readability of messages. [Commit.](http://commits.kde.org/spacebar/27b884f292eff2517fcd04de3811a58382c4e390) 
+ Improve new conversation selection page. [Commit.](http://commits.kde.org/spacebar/3807eba2d25f9583bb9caa999e23929e41583e51) 
+ Improve chats display. [Commit.](http://commits.kde.org/spacebar/fd8f3e21b6e1aea50b46fd733f9a95dd5ff4d47f) 
+ Update syntax because implicitly defined onFoo properties in Connections are deprecated. [Commit.](http://commits.kde.org/spacebar/b1dd3ebd9d9fe9cbe60a1c9d8ee84f3e0a79db94) 
+ Normalize numbers for Ofono. [Commit.](http://commits.kde.org/spacebar/bbf06092209dbc47ff5b53a0cdbf8afe7a45c3b6) 
{{< /details >}}

{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Make urls in toots clickable. [Commit.](http://commits.kde.org/tokodon/feb3b6216d978a90bdada4a16beae04ba777b8b4) 
+ Fix #13: Add buttons to open/copy auth link. Improve Android workflow. [Commit.](http://commits.kde.org/tokodon/b8fd218be5d2907ef0ba18d5b7d0693c51fbcf8a) 
+ Fix QML syntax error. [Commit.](http://commits.kde.org/tokodon/8a5aebd57b49a5a439fc2f81231562b208e50ca3) 
+ Add initial support to Content Warning. [Commit.](http://commits.kde.org/tokodon/85815920d113d51f2e5cb191c5014c65bfc7b0c6) 
+ Call static Q[Core|Gui]Application methods on the correct types. [Commit.](http://commits.kde.org/tokodon/eb4d447fd9746a4a03dc6646ef3ff269349fb87d) 
+ Don't include QApplication on Android. [Commit.](http://commits.kde.org/tokodon/badf7cd0e6f5b887b8da34636627cb36ba23bc23) 
+ Add Androidmanifest. [Commit.](http://commits.kde.org/tokodon/b8d7ab8cb9c55ac70f7e12fddb2fd43c5dd22ec9) 
{{< /details >}}
