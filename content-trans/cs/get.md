---
menu:
  main:
    name: Instalace
    weight: 4
sassFiles:
- scss/get.scss
title: Distributions offering Plasma Mobile
---
## Mobilní

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM is the Manjaro distribution, but for ARM devices. It's based on Arch Linux ARM, combined with Manjaro tools, themes and infrastructure to make install images for your ARM device.

[Website](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Ke stažení

* [Latest Stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Developer builds (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalace

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that can be installed on smartphones and other mobile devices. View the [device list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for supporting your device.

For devices that do not have prebuilt images, you will need to flash it manually using the `pmbootstrap` utility. Follow instructions [here](https://wiki.postmarketos.org/wiki/Installation_guide). Be sure to also check the device's wiki page for more information on what is working.

[Learn more](https://postmarketos.org)

##### Ke stažení

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Latest Edge (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Community Devices](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux distribution sponsored by SUSE Linux GmbH and other companies. Currently openSUSE provides Tumbleweed based Plasma Mobile builds.

##### Ke stažení

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instalace

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Desktop Devices

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Neon based amd64 ISO image

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
