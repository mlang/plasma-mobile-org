---
jsFiles:
- https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
layout: page
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/screenshots.scss
- scss/components/swiper.scss
screenshots:
- name: Plasma mobile homescreen
  url: /screenshots/plasma.png
- name: KWeather, mobilní aplikace pro počasí
  url: /screenshots/weather.png
- name: Kalk, aplikace kalkulačky
  url: /screenshots/pp_calculator.png
- name: Mexapixels, aplikace pro fotoaparát
  url: /screenshots/pp_camera.png
- name: Calindori, aplikace kalendáře
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, aplikace pro poznámky
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, univerzální prohlížeč dokumentů
  url: /screenshots/pp_okular01.png
- name: Angelfish, webový prohlížeč
  url: /screenshots/pp_angelfish.png
- name: Nota, textový editor
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, další prohlížeč obrázků
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, Správce souborů
  url: /screenshots/pp_folders.png
- name: Wave, přehrávač hudby
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Hardware
  url: /screenshots/20201110_092718.jpg
title: Obrázky
---
The following screenshots were taken from a Pinephone device running Plasma Mobile.

{{< screenshots name="screenshots" >}}
