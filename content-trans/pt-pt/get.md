---
menu:
  main:
    name: Instalar
    weight: 4
sassFiles:
- scss/get.scss
title: Distribuições que oferecem o Plasma Mobile
---
## Telemóvel

### Manjaro ARM

![](/img/manjaro.svg)

O Manjaro ARM é a distribuição Manjaro, mas para dispositivos ARM. Baseia-se no Arch Linux ARM, combinado com as ferramentas, temas e infra-estruturas do Manjaro para criar imagens de instalação para o seu dispositivo ARM.

[Página Web](https://manjaro.org) [Fórum](https://forum.manjaro.org/c/arm/)

##### Obter

* [Última Versão Estável (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Versões de desenvolvimento (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalação

Para o PinePhone, poderá encontrar informações genéricas na [wiki do Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

O PostmarketOS (pmOS) é um Alpine Linux optimizado para dispositivos tácteis e pré-configurado que poderá ser instalado em telemóveis e noutros dispositivos móveis. Consulte a [lista de dispositivos](https://wiki.postmarketos.org/wiki/Devices) para saber a evolução do suporte para o seu dispositivo.

Para os dispositivos que não têm imagens pré-compiladas, terá de a gravar manualmente com o utilitário `pmbootstrap`. Siga as instruções [aqui](https://wiki.postmarketos.org/wiki/Installation_guide). Certifique-se que também consulta a página da wiki do dispositivo para saber o que está a funcionar.

[Saber mais](https://postmarketos.org)

##### Obter

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Última Versão (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Dispositivos da Comunidade](https://postmarketos.org/download/)
* [Lista de Dispositivos Completa](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

O openSUSE, conhecido antigamente como SUSE Linux ou SuSE Linux Professional, é uma distribuição de Linux patrocinada pela SUSE Linux GmbH e por outras empresas. De momento, o openSUSE fornece versões do Plasma Mobile baseadas no Tumbleweed.

##### Obter

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instalação

Para o PinePhone, poderá encontrar informações genéricas na [wiki do Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Computadores Pessoais

### postmarketOS

![](/img/pmOS.svg)

O postmarketOS é capaz de correr no QEMU, sendo assim uma opção adequada para experimentar o Plasma Mobile no seu computador.

Leia mais sobre ele [aqui](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Durante o processo de configuração, basta seleccionar o Plasma Mobile como ambiente de trabalho.

### Imagem ISO em amd64 baseada no Neon

![](/img/neon.svg)

ATENÇÃO: Isto não está em manutenção activa de momento!

Esta imagem, baseada no Neon, pode ser testada em 'tablets' não-Android da Intel, PC's e máquinas virtuais.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
