---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Juntar-se ao Plasma Mobile
---
Se gostaria de contribuir para este 'software' espectacular para dispositivos móveis, [junte-se a nós - teremos sempre uma tarefa para si](/contributing/)!

Grupos e canais da comunidade do Plasma Mobile:

### Canais específicos do Plasma Mobile:

* [![](/img/matrix.svg)Matrix (mais activo)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Lista de correio do Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canais do projecto relacionados com o Plasma Mobile:

* [![](/img/irc.png)#plasma no Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium no Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Lista de correio de desenvolvimento do Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
