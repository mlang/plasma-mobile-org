---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: 加入 Plasma Mobile
---
如果您想要为 Plasma Mobile 贡献力量，请[加入我们](/contributing/)！

Plasma Mobile 社区工作小组和聊天频道：

### Plasma Mobile 专属频道：

* [![](/img/matrix.svg)Matrix (最为活跃)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Plasma Mobile 邮件列表](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile 相关项目频道：

* [![](/img/irc.png)#Plasma 的 Freenode 聊天室 (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium 的 Telegram 群组](https://t.me/Halium)
* [![](/img/mail.svg)Plasma 开发邮件列表](https://mail.kde.org/mailman/listinfo/plasma-devel)
