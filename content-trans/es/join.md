---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Únase a Plasma Mobile
---
Si desea colaborar con el increíble software libre para dispositivos móviles, ¡[únase a nosotros, siempre tenemos una tarea para usted](/contributing/)!

Grupos y canales de la comunidad de Plasma Mobile:

### Canales específicos de Plasma Mobile:

* [![](/img/matrix.svg)Matrix (más activo)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Lista de distribución de Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canales de proyectos relacionados con Plasma Mobile:

* [![](/img/irc.png)#plasma en Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium en Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Lista de distribución del desarrollo de Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
