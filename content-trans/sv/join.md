---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Gå med i Plasma Mobil
---
Om du vill bidra till den enastående fria programvaran för mobilapparater, [gå med - det finns alltid en uppgift för dig](/contributing/).

Plasma Mobile gemenskapsgrupper och kanaler:

### Plasma Mobil-specifika kanaler:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobil-relaterade projektkanaler:

* [![](/img/irc.png)#plasma på Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium på Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Plasma e-postlista för utvecklare](https://mail.kde.org/mailman/listinfo/plasma-devel)
