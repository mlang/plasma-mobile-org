---
layout: page
menu:
  main:
    name: Svar på vanliga frågor
    parent: project
    weight: 5
title: Svar på vanliga frågor
---
Varför använder inte Plasma Mobil Mer eller Nemomobile?
-
Plasma Mobil är en programvaruplattform för mobiltelefoner. Det är inte ett operativsystem i sig självt. Det består av Qt5, KDE Ramverk, Plasma och diverse programvara som ingår i programuppsättningen. Plasma Mobil kan fungera med Mer-distributionen, men på grund av tids- och resursbrist, fokuserar vi för närvarande på KDE Neon på PinePhone som bas för utprovning och utveckling.

Kan Android applikationer fungera på Plasma Mobil?
-
Det finns projekt som [Anbox](https://anbox.io/), som är Android inne i en Linux-behållare, som använder Linux-kärnan för att köra program som uppnår nästan inbyggd prestanda. Det kan utnyttjas i framtiden för att få Android applikationer att köra ovanpå ett GNU/Linux-system med Plasma Mobile-plattformen, men det är en komplicerad uppgift, och *idag* (6:e september, 2020) stöder vissa distributioner redan Anbox och du kan köra Plasma Mobil med dessa distributioner.

Kan jag köra Plasma Mobil på min mobiltelefon?
-
För närvarande kör Plasma Mobil på följande typer av telefon:

* **(Rekommenderas) PinePhone:** Vi erbjuder officiella avbilder byggda för PinePhone med KDE Neon. Du hittar mer information på Plasma Mobil [documentation](https://docs.plasma-mobile.org).
* **x86-baserade:** Om du vill prova Plasma Mobil på en Intel surfplatta, skrivbordsdator, bärbar dator, eller virtuell maskin, är den x86_64 Neon-baserade Plasma Mobil [avbilden](https://www.plasma-mobile.org/get/) till för dig. Information om hur man installerar den permanent finns i [dokumentationen](https://docs.plasma-mobile.org) av Plasma Mobil.
* **postmarketOS-apparater:** postmarketOS är en distribution baserad på Alpine Linux som kan installeras på Android smarta telefoner och andra apparater. Projektet erbjuder stöd för ett ganska stort antal apparater, och erbjuder Plasma Mobil som ett tillgängligt gränssnitt. Sök efter din apparat i [listan över apparater som stöds](https://wiki.postmarketos.org/wiki/Devices) för att se vad som fungerar. Därefter kan du följa [pmOS installationshandledning](https://wiki.postmarketos.org/wiki/Installation_guide) för att installera det på din apparat. Det kan fungera olika bra beroende på apparaten som används, och när du använder en apparat i testkategorin är det **inte** nödvändigtvis representativt för Plasma Mobils nuvarande status.
* **Övriga:** Tyvärr har stöd för Halium-baserade enheter nyligen tagits bort (se [teknisk skuld](/2020/12/14/plasma-mobile-technical-debt.html)). Det inkluderar den tidigare referensenheten Nexus 5x.

Jag har installerat Plasma Mobil, vad är inloggningslösenordet?
-
Om du har installerat Neon på din PinePhone via installationsskriptet, ska lösenordet vara "1234", och du kan sedan ändra det efteråt genom att köra "passwd" i Terminal. För Manjaro är det 123456. När du ändrar det, kom ihåg att du för närvarande bara kan skriva in siffror på låsningsskärmen.

Om du använder x86-avbilden är inget lösenord normalt inställt, och du måste ange det genom att köra "passwd" i Terminal innan du kan utföra behörighetskontroll för någonting.

Vad är projektets status?
-
Plasma Mobil är för närvarande under kraftig utveckling, och är inte avsett för daglig användning. Om du är intresserad av att bidra, [gå med](/findyourway) i spelet.
