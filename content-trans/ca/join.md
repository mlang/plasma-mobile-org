---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Uniu-vos al Plasma Mobile
---
Si us agradaria col·laborar amb l'impressionant programari lliure per a dispositius mòbils, [uniu-vos - sempre hi ha una tasca per a vós](/contributing/)!

Grups i canals de la comunitat Plasma Mobile:

### Canals específics del Plasma Mobile:

* [![](/img/matrix.svg)Matrix (més actiu)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Llista de correu del Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canals de projectes relacionats amb el Plasma Mobile:

* [![](/img/irc.png)#plasma al Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium al Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Llista de correu de desenvolupament del Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
