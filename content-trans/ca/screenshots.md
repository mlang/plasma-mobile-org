---
jsFiles:
- https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
layout: page
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/screenshots.scss
- scss/components/swiper.scss
screenshots:
- name: Pantalla d'inici del Plasma Mobile
  url: /screenshots/plasma.png
- name: KWeather, l'aplicació de meteorologia del Plasma Mobile
  url: /screenshots/weather.png
- name: Kalk, una aplicació calculadora
  url: /screenshots/pp_calculator.png
- name: Megapixels, una aplicació de càmera
  url: /screenshots/pp_camera.png
- name: Calindori, una aplicació de calendari
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, una aplicació per a prendre notes
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, un visor universal de documents
  url: /screenshots/pp_okular01.png
- name: Angelfish, un navegador web
  url: /screenshots/pp_angelfish.png
- name: Nota, un editor de text
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, un altre visor d'imatges
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, un gestor de fitxers
  url: /screenshots/pp_folders.png
- name: VVave, un reproductor de música
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: El maquinari
  url: /screenshots/20201110_092718.jpg
title: Captures de pantalla
---
Les captures de pantalla següents s'han pres des d'un dispositiu Pinephone executant el Plasma Mobile.

{{< screenshots name="screenshots" >}}
