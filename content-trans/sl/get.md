---
menu:
  main:
    name: Namesti
    weight: 4
sassFiles:
- scss/get.scss
title: Distribucije, ki nudijo Plasma Mobile
---
## Mobilci

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM je distribucija Manjaro za ARM naprave. Temelji na Arch Linux ARM v kombinaciji z orodji, temami in infrastrukturo Manjaro za izdelavo namestitvenih datotek za vašo napravo ARM.

[Spletišče](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Prenos

* [Zadnji stabilni (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Razvojne verzije (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Namestitev

Za PinePhone lahko najdete generične informacije na [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) je vnaprej sestavljen Alpine Linux, optimiziran za rabo z dotikanjem, ki ga je mogoče namestiti na pametne telefone in druge mobilne naprave. Poglejte [seznam naprav](https://wiki.postmarketos.org/wiki/Devices) za ogled napredka podpore vaše naprave.

Za naprave, ki nimajo vnaprej izdelanih namestitvenih slik, jih boste morali napisati ročno s pripomočkom `pmbootstrap`. Sledite navodilom [tukaj](https://wiki.postmarketos.org/wiki/Installation_guide). Preverite tudiwiki strani naprave za več informacij o tem, kaj od programja deluje.

[Learn more](https://postmarketos.org)

##### Prenos

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Zadnji Edge (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Skupne naprave](https://postmarketos.org/download/)
* [Celotni seznam naprav](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, prej SUSE Linux in SuSE Linux Professional, je distribucija Linuxa, ki jo sponzorira SUSE Linux GmbH in druga podjetja. Trenutno openSUSE ponuja sestave Plasma Mobile na osnovi sistema Tumbleweed.

##### Prenos

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Namestitev

Za PinePhone lahko najdete generične informacije na [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Namizne naprave

### postmarketOS

![](/img/pmOS.svg)

postmarketOS je mogoče poganjati v QEMU in tako je primerna možnost za poskus rabe Plasma Mobile na vašem računalniku.

Preberite si več o tem [tukaj](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Med postopkom nameščanja enostavno izberite Plasma Mobile kot namizno okolje.

### ISO datoteka na osnovi Neona za amd64

![](/img/neon.svg)

OPOZORILO: To ni aktivno podprto!

To datoteko ISO, ki temelji na KDE Neonu lahko preizkusite na ne-androidnih tabličnih računalnikih Intel, osebnih računalnikih in navideznih računalnikih.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
