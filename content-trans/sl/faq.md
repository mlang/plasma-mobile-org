---
layout: page
menu:
  main:
    name: FAQ (PZV)
    parent: project
    weight: 5
title: Pogosto zastavljena vprašanja
---
Zakaj Plasma Mobile ne uporablja Mer/Nemomobile?
-
Plasma Mobile je programska platforma za mobilne naprave. Ni operacijski sistem sam po sebi, sestavljajo ga Qt5, KDE Frameworks, Plasma in različna programska oprema, ki je del nabora aplikacij. Plasma Mobile lahko deluje na distribuciji Mer, vendar se zaradi pomanjkanja časa in virov trenutno osredotočamo na KDE Neon na PinePhone kot osnovo za testiranje in razvoj.

Ali lahko aplikacije za Android delujejo na Plasma Mobile?
-
Obstajajo projekti, kot je [Anbox] (https://anbox.io/), ki je Android, ki deluje znotraj vsebnika Linux in za izvajanje programov uporablja jedro Linuxa za izvajanje aplikacij v skoraj domači zmogljivosti. To bi lahko v prihodnosti izkoristili za aplikacije za Android, ki se izvajajo na sistemu GNU/Linux na platformi Plasma Mobile, vendar je to zapletena naloga in od *danes* (6. september 2020) nekatere distribucije že podpirajo Anbox in lahko zaženete Plasmo Mobile na teh distribucijah.

Ali lahko na svoji mobilni napravi poganjam Plasma Mobile?
-
Trenutno Plasma Mobile deluje na naslednjih vrstah naprav:

* **(Priporočljivo) PinePhone:** Nudimo uradne slike, izdelane za PinePhone na KDE Neon. Več informacij o Plasmi Mobile [dokumentacija] (https://docs.plasma-mobile.org).
* **na osnovi x86:** Če želite preizkusiti Plasma Mobile v tabličnem računalniku Intel, namiznem/prenosnem ali v navideznem računalniku x86_64 je za vas Plasma Mobile na osnovi ditribucije Neon [slika] (https://www.plasma-mobile.org/get/). Informacije o tem, kako za trajno namestitev je na voljo na Plasma Mobile [dokumentacija] (https://docs.plasma-mobile.org).
* **naprave postmarketOS:** postmarketOS je prednastavljen sistem optimiziran za dotik Alpine Linux, ki ga lahko namestite na pametne telefone Adnroid in druge mobilne naprave. Ta projekt nudi podporo razmeroma širokemu naboru naprav in nudi Plasma Mobile kot eno od možnih vmesnikov. Poiščite svojo napravo na [seznamu podprtih naprav] (https://wiki.postmarketos.org/wiki/Devices) in preverite, kaj deluje, potem pa sledite [pmOS installation guide] (https://wiki.postmarketos.org/wiki/Installation_guide) za namestitev na vaši napravi. Vaša kilometrina je lahko zelo različna in to **ni** nujno predstavitev trenutnega stanja  Plasma Mobile.
* **Ostalo:** Na žalost je bilo treba opustiti podporo za naprave Halium (glejte [technical debt](/2020/12/14/plasma-mobile-technical-debt.html)). To vsebuje tudi prejšnja sklicevanja na napravo Nexus 5x.

Namestil sem Plasma Mobile, kaj je geslo za prijavo?
-
Če ste Neon namestili na svoj PinePhone prek namestitvenega skripta, mora biti geslo "1234", nato pa ga lahko nato spremenite z zagonom "passwd" v Konsoli. Za Manjaro je 123456. Ko ga spremenite, ne pozabite, da lahko trenutno na zakljenjen zaslon vnašate samo številke.

Če uporabljate sliko x86, privzeto ni nastavljeno nobeno geslo in ga boste morate nastaviti tako, da v programu Konsole zaženete "passwd", preden lahko se lahko overite za karkoli.

Kakšno je stanje projekta?
-
Plasma Mobile je trenutno v zelo intenzivnem razvoju in ni namenjen za dnevno rabo. Če želite kaj prispevati, [se pridružite] (/findyourway) v igri.
