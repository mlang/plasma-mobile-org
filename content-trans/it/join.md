---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Unisciti a Plasma Mobile
---
Se desideri contribuire allo straordinario software libero per dispositivi mobili, [unisciti - abbiamo sempre qualcosa da farti fare](/contributing/)!

Gruppi e canali della comunità di Plasma Mobile:

### Canali specifici di Plasma Mobile:

* [![](/img/matrix.svg)Matrix (il più attivo)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Lista di distribuzione di Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canali di progetti relativi a Plasma Mobile:

* [![](/img/irc.png)#plasma su Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium su Telegram](https://t.me/Halium)
* [![](/img/mail.svg)lista di distribuzione sullo sviluppo di Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
