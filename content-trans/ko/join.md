---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Plasma 모바일에 참여하기
---
모바일 장치에 탑재되는 자유 소프트웨어에 기여하고 싶으시다면 [지금 참여하세요! 여러분의 작업은 항상 도움이 됩니다!](/contributing/)

Plasma 모바일 커뮤니티 그룹과 채널:

### Plasma 모바일 전용 채널:

* [![](/img/matrix.svg)Matrix(가장 활성화됨)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)텔레그램](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Plasma 모바일 메일링 리스트](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma 모바일 관련 프로젝트 채널:

* [![](/img/irc.png)Freenode(IRC)의 #plasma 채널](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)텔레그램의 Halium](https://t.me/Halium)
* [![](/img/mail.svg)Plasma 개발 메일링 리스트](https://mail.kde.org/mailman/listinfo/plasma-devel)
