---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Rejoignez Plasma Mobile
---
Si vous souhaitez contribuer à un incroyable logiciel pour périphériques mobiles, [rejoignez nous - il y aura toujours du travail pour vous](/contributing/) !

Groupes et canaux de la communauté de Plasma Mobile :

### Canaux spécifiques de Plasma Mobile :

* [![](/img/matrix.svg)Matrix (la plus active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Liste de discussions de Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canaux concernant les projets relatifs à Plasma Mobile :

* [![](/img/irc.png)#plasma sur Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium sur Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Liste de discussion pour le développement de Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
