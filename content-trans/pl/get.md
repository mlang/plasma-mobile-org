---
menu:
  main:
    name: Wgraj
    weight: 4
sassFiles:
- scss/get.scss
title: Dystrybucje dostarczające Przenośną Plazmę
---
## Przenośna

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM jest dystrybucją Manjaro, lecz dla urządzeń ARM. Jest oparta na Arch Linux ARM, w połączeniu z narzędziami Manjaro, wyglądem i infrastrukturą, aby móc wgrywać obrazy na twoje urządzenia ARM.

[Strona sieciowa](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Pobierz

* [Najnowsze stabilne (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Wydania rozwojowe (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Wgrywanie

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), jest urządzeniem dostosowanym do dotyku z wstępnie ustawionym Alpine Linux, którego można wgrać na smartfonach przeznaczonych dla Androida i na innych urządzeniach przenośnych. Na [wykazie urządzeń](https://wiki.postmarketos.org/wiki/Devices) zobaczysz postęp we wsparciu dla twojego urządzenia.

Dla urządzeń, które nie mają uprzednio zbudowanych obrazów, będziesz musiał wgrać to ręcznie przy użyciu narzędzia `pmbootstrap`. Przeczytaj szczegóły [tutaj](https://wiki.postmarketos.org/wiki/Installation_guide). Przeczytaj także stronę wiki urządzeń, aby dowiedzieć się co działa.

[Dowiedz się więcej](https://postmarketos.org)

##### Pobierz

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Najnowsze rozwojowe(Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Urządzenia społeczności](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, poprzednio SUSE Linux oraz SuSE Linux Professional, jest dystrybucją Linuksa sponsorowaną przez SUSE Linux GmbH i inne przedsiębiorstwa. ObecnieopenSUSE dostarcza Tumbleweed na podstawie zbudowanej Przenośnej Plazmy.

##### Pobierz

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Wgrywanie

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Urządzenia biurkowe

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Obraz ISO amd64 oparty na Neonie

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
