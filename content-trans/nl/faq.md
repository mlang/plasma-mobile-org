---
layout: page
menu:
  main:
    name: FAQ
    parent: project
    weight: 5
title: Veel voorkomende vragen
---
Waarom gebruikt Plasma Mobile geen Mer/Nemomobile?
-
Plasma Mobile is een software platform voor mobiele apparaten. Het is geen besturingssysteem in zichzelf, het bestaat uit Qt5, het KDE Frameworks, Plasma en een verscheidenheid aan software dat onderdeel is van de set toepassingen. Plasma Mobile kan bovenop de Mer distributie werken, maar vanwege gebrek aan tijd en hulpbronnen richten we focus op KDE Neon op PinePhone als een basis voor testen en ontwikkeling.

Kunnen Android apps op Plasma Mobile werken?
-
Er zijn projecten zoals [Anbox](https://anbox.io/) wat Android is draaiend in een Linux container en de Linux kernel gebruiken om toepassingen uit te voeren om bijna inheemse prestaties te bereiken. Dit zou in de toekomst omhoog gebracht kunnen worden naar have Android apps die draaien bovenop een GNU/Linux systeem met het Plasma Mobile platform, maar het is een gecompliceerde taak en vanaf *vandaag*(6 september 2020) ondersteunen sommige distributies al Anbox en kunt u Plasma Mobile draaien bovenop deze distributies.

Kan ik Plasma Mobile op mijn mobiele apparaat draaien?
-
Op dit moment draait Plasma Mobile op de volgende typen apparaten:

* **(Aanbevolen) PinePhone:** We bieden officiële images gebouwd voor de PinePhone bovenop KDE Neon. U kunt meer informatie vinden op de Plasma Mobile [documentatiesite](https://docs.plasma-mobile.org).
* **x86-based:** Als u Plasma Mobile wilt uitproberen op een Intel tablet, desktop/laptop of virtuele machine, dan is de x86_64 op Neon gebaseerde Plasma Mobile [image](https://www.plasma-mobile.org/get/) er voor u. Informatie over hoe het permanent te installeren is te vinden op de Plasma Mobile [documentatiesite](https://docs.plasma-mobile.org).
* **postmarketOS apparaten:** postmarketOS is een distributie gebaseerd op Alpine Linux dat geïnstalleerd kan worden op Android smarttelefoons en andere mobiele apparaten. Dit project biedt ondersteuning voor een tamelijk brede reeks apparaten en het biedt Plasma Mobile als een beschikbaar interface. Zoek uw apparaat op de [lijst met ondersteunde apparaten](https://wiki.postmarketos.org/wiki/Devices) en zie wat er werkt, daarna kunt u de [pmOS installatiegids](https://wiki.postmarketos.org/wiki/Installation_guide) volgen om het te installeren op uw apparaat. Het resultaat kan variëren, afhankelijk van het gebruikte apparaat en wanneer u een apparaat in de testcategorie gebruikt is **niet** noodzakelijk representatief voor de huidige staat van Plasma Mobile.
* **Andere:** Helaas moest ondersteuning voor op Halium gebaseerde apparaten recent gestopt worden (zie [technische schuld](/2020/12/14/plasma-mobile-technical-debt/)). Dit omvat het eerdere referentie apparaat Nexus 5x.

Ik heb Plasma Mobile geïnstalleerd, wat is het wachtwoord voor aanmelden?
-
Als u Neon op uw PinePhone via het installatiescript hebt geïnstalleerd zou het wachtwoord "1234" moeten zijn en u kunt het daarna wijzigen met uitvoeren van "passwd" in Konsole. Voor Manjaro is het 123456. Bij wijzigen, bedenk dat u op dit moment alleen cijfers op het vergrendelscherm kunt invoeren.

Als u de x86 image gaat gebruiken is er standaard geen wachtwoord ingesteld en u moet het instellen door "passwd" uit te voeren in Konsole alvorens u iets kunt authentiseren.

Wat is de status van het project?
-
Plasma Mobile is op dit moment zwaar in ontwikkeling en is niet bedoeld voor dagelijks gebruik. Als u geïnteresseerd bent in bijdragen, [doe mee](/findyourway) in het spel.
