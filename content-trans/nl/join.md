---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Doe mee met Plasma Mobile
---
Als u wilt bijdragen aan de verbazingwekkende vrije software voor mobiele apparaten, [doe mee - we hebben altijd een taak voor u](/contributing/)!

Plasma Mobile gemeenschapsgroepen en kanalen:

### Plasma Mobile specifieke kanalen:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile gerelateerde projectkanalen:

* [![](/img/irc.png)#plasma op Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium on Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Plasma ontwikkeling e-maillijst](https://mail.kde.org/mailman/listinfo/plasma-devel)
