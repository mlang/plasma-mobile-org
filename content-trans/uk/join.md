---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Долучитися до Plasma Mobile
---
Якщо ви хочете взяти участь у розробці чудового вільного програмного забезпечення для мобільних пристроїв, [долучайтеся до нас — у нас завжди знайдеться для вас завдання](/contributing/)!

Групи і канали спільноти Plasma Mobile:

### Спеціалізовані канали Plasma Mobile:

* [![](/img/matrix.svg)Matrix (найактивніший канал)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)
* [![](/img/mail.svg)Список листування Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Канали, які пов'язано із Plasma Mobile:

* [![](/img/irc.png)#plasma у Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)
* [![](/img/telegram.svg)Halium у Telegram](https://t.me/Halium)
* [![](/img/mail.svg)Список листування розробників Плазми](https://mail.kde.org/mailman/listinfo/plasma-devel)
